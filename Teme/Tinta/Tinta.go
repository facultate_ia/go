package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main(){
	fmt.Println("Raza: ")
	var r float64
	fmt.Scan(&r)

	fmt.Println("Numar trageri: ")
	var nr float64
	fmt.Scan(&nr)

	min := 0.
	max := 2. * r
	m := 0.

	rand.Seed(time.Now().UnixNano())
	for i := 0; i < int(nr); i++ {
		x := (rand.Float64() * (max - min)) + min
		y := (rand.Float64() * (max - min)) + min

		if (x-r)*(x-r)+(y-r)*(y-r) <= r*r {
			m++
		}
	}
	fmt.Println("Trageri in cerc: ", m)
	pi := 4. * m / nr
	fmt.Println(pi)
}
