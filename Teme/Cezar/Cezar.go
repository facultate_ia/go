package main

import "fmt"

func rotate(text string, shift int) string {
	shift = (shift%26 + 26) % 26
	b := make([]byte, len(text))
	for i := 0; i < len(text); i++ {
		t := text[i]
		var a int
		switch {
		case 'a' <= t && t <= 'z':
			a = 'a'
		case 'A' <= t && t <= 'Z':
			a = 'A'
		default:
			b[i] = t
			continue
		}
		b[i] = byte(a + ((int(t)-a)+shift)%26)
	}
	return string(b)
}

func encode(plain string, shift int) (cipher string) {
	return rotate(plain, shift)
}

func decode(cipher string, shift int) (plain string) {
	return rotate(cipher, -shift)
}

func main() {
	var k = 5
	var text = "secret text"
	fmt.Println("Text:", text)
	var encodedString = encode(text, k)
	fmt.Println("Encoded text:", encodedString)
	fmt.Println("Decoded text:", decode(encodedString, k))
}
