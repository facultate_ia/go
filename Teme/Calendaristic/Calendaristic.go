package main

type Date struct {
	year  int
	month int
	day   int
}

func (d1 Date) getNumberOfDays(d2 Date) int {
	years := 0
	months := 0
	days := 0
	if d1.year < d2.year {
		years = d2.year - d1.year
		if d1.month < d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 30 - (d1.day - d2.day)
				months--
			}
		} else if d1.month == d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 12*30 - (d1.day - d2.day)
				years--
			}
		} else {
			years--
			months = 12 - (d1.month - d2.month)
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 30 - (d1.day - d2.day)
				months--
			}
		}
	} else if d1.year == d2.year {
		years = d2.year - d1.year
		if d1.month < d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				months--
				days = 30 - (d1.day - d2.day)
			}
		} else if d1.month == d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		} else {
			months = d1.month - d2.month
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		}
	} else {
		years = d1.year - d2.year
		if d1.month < d2.month {
			years--
			months = 12 - (d2.month - d1.month)
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 30 - (d1.day - d2.day)
			}
		} else if d1.month == d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months = 11
				years--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		} else {
			months = d1.month - d2.month
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		}
	}
	return days + months * 30 + years * 12 * 30
}

func (d1 Date) addDays(days int) Date {
	if days >= 365 {
		d1.year += days / 365
		days -= 365 * days / 365
	}
	if days >= 30 {
		d1.month += days / 30
		days -= 30 * days / 30
	}
	if d1.month > 12 {
		d1.year++
		d1.month -= 12
	}
	d1.day += days
	if d1.day > 30 {
		d1.month++
		d1.day -= 30
	}
	return d1
}

func (d1 Date) getTimeBetweenDates(d2 Date) Date {

	years := 0
	months := 0
	days := 0
	if d1.year < d2.year {
		years = d2.year - d1.year
		if d1.month < d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 30 - (d1.day - d2.day)
				months--
			}
		} else if d1.month == d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 12*30 - (d1.day - d2.day)
				years--
			}
		} else {
			years--
			months = 12 - (d1.month - d2.month)
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 30 - (d1.day - d2.day)
				months--
			}
		}
	} else if d1.year == d2.year {
		years = d2.year - d1.year
		if d1.month < d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				months--
				days = 30 - (d1.day - d2.day)
			}
		} else if d1.month == d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = d2.day - d1.day
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		} else {
			months = d1.month - d2.month
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		}
	} else {
		years = d1.year - d2.year
		if d1.month < d2.month {
			years--
			months = 12 - (d2.month - d1.month)
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = 30 - (d1.day - d2.day)
			}
		} else if d1.month == d2.month {
			months = d2.month - d1.month
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months = 11
				years--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		} else {
			months = d1.month - d2.month
			if d1.day < d2.day {
				days = 30 - (d2.day - d1.day)
				months--
			} else if d1.day == d2.day {
				days = d2.day - d1.day
			} else {
				days = d1.day - d2.day
			}
		}
	}
	return Date{day: days, month: months, year: years}
}

func AnBisect (an int) bool{
	if an % 100==0 && an%400==0{
		return true
	}
	if an%100!=0 && an%4==0{
		return true
	}
	return false
}

func main() {
	d1 := Date{2023, 5, 9}
	d2 := Date{2020, 2, 20}
	print("1. Numarul de zile dintre 2 date calendaristice: ", d1.getNumberOfDays(d2), "\n")

	d3 := d1.getTimeBetweenDates(d2)
	print("2. Timpul scurs dintre 2 date calendaristice calculat in ani, luni, saptamani, zile:\n", d3.year, " ani - ", d3.month, " luni - ", d3.day / 7, " saptamani - ", d3.day % 7, " zile\n")

	d := d1.addDays(29)
	print("3. Data +/- un nr specificat de zile: ", d.year, "-", d.month, "-", d.day)
}
