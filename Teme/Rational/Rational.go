package main

import (
	"math"
)

type Rational struct {
	Numarator, Numitor int
}

func (r Rational) getNumarator() int {
	return r.Numarator
}

func (r Rational) getNumitor() int {
	return r.Numitor
}

func (r Rational) newInstance(x, y int) Rational {
	t := Rational{x, y}
	return t
}

func (r Rational) add(t Rational) Rational {
	return Rational{Numarator: cmmmc(r.Numitor, t.Numitor) / r.Numitor * r.Numarator + cmmmc(r.Numitor, t.Numitor) / t.Numitor * t.Numarator, Numitor: cmmmc(r.Numitor, t.Numitor)}
}

func (r Rational) substract(t Rational) Rational {
	return Rational{Numarator: cmmmc(r.Numitor, t.Numitor) / r.Numitor - cmmmc(r.Numitor, t.Numitor) / t.Numitor, Numitor: cmmmc(r.Numitor, t.Numitor)}
}

func (r Rational) multiply(t Rational) Rational {
	return Rational{Numarator: r.Numarator * t.Numitor, Numitor: r.Numitor * t.Numitor}
}

func (r Rational) multiplyInt(t int) Rational {
	return Rational{Numarator: r.Numarator * t, Numitor: r.Numitor}
}

func (r Rational) divideInt(t int) Rational {
	return Rational{Numarator: r.Numarator, Numitor: r.Numitor * t}
}

func (r Rational) substractInt(t int) Rational {
	return Rational{Numarator: r.Numarator - t*r.Numitor, Numitor: r.Numitor}
}

func (r Rational) addInt(t int) Rational {
	return Rational{Numarator: r.Numarator + t*r.Numitor, Numitor: r.Numitor}
}

func (r Rational) addNuma(t int) Rational {
	return Rational{Numarator: r.Numarator + t, Numitor: r.Numitor}
}

func (r Rational) addNumi(t int) Rational {
	return Rational{Numarator: r.Numarator, Numitor: r.Numitor + t}
}

func (r Rational) addNumaAndNumi(t int) Rational {
	return Rational{Numarator: r.Numarator + t, Numitor: r.Numitor + t}
}

func (r Rational) substractNuma(t int) Rational {
	return Rational{Numarator: r.Numarator - t, Numitor: r.Numitor}
}

func (r Rational) substractNumi(t int) Rational {
	return Rational{Numarator: r.Numarator, Numitor: r.Numitor - t}
}

func (r Rational) substractNumaAndNumi(t int) Rational {
	return Rational{Numarator: r.Numarator - t, Numitor: r.Numitor - t}
}

func (r Rational) isNull() bool {
	return r.Numarator == 0
}

func (r Rational) getRealValue() float32 {
	return float32(r.Numarator / r.Numitor)
}

func (r Rational) getAbsValue() Rational {
	return Rational{Numarator: int(math.Abs(float64(r.Numarator))), Numitor: int(math.Abs(float64(r.Numitor)))}
}

func (r Rational) divide(t Rational) Rational {
	return Rational{Numarator: r.Numarator * t.Numitor, Numitor: t.Numarator * r.Numitor}
}

func (r Rational) simplify() Rational {
	c := cmmdc(r.Numarator, r.Numitor)
	r.Numarator /= c
	r.Numitor /= c
	return r
}

func (r Rational) pow(n int) Rational {
	return Rational{Numarator: r.Numarator * n, Numitor: r.Numitor * n}
}

func (r Rational) bigerThan(t Rational) bool {
	return cmmmc(r.Numitor, t.Numitor) / r.Numitor * r.Numarator > cmmmc(t.Numitor, r.Numitor) / t.Numitor * t.Numarator
}

func (r Rational) smallerThan(t Rational) bool {
	return cmmmc(r.Numitor, t.Numitor) / r.Numitor * r.Numarator < cmmmc(t.Numitor, r.Numitor) / t.Numitor * t.Numarator
}

func (r Rational) equals(t Rational) bool {
	return r.simplify().Numarator == t.simplify().Numarator && r.simplify().Numitor == t.simplify().Numitor
}

// ?
func (r Rational) inverse() bool {
	return true
}

func isIntegral(val float32) bool {
	return val == float32(int(val))
}

func (r Rational) isNatural() bool {
	return r.getRealValue() >= 0 && isIntegral(r.getRealValue())
}

// 0.4324 => 4324 / 1000 => simplificare
func getFromFloat32(x float32) Rational {
	return Rational{0, 1}
}

func cmmdc(a, b int) int {
	for a%b != 0 {
		r := a % b
		a = b
		b = r
	}
	return b
}

func cmmmc(a, b int) int {
	return a * b / cmmdc(a, b)
}

func main() {

}