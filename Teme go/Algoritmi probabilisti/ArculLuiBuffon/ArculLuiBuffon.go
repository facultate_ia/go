package ArculLuiBuffon

import (
	"fmt"
	"math"
	"math/rand"
)

func ArculLuiBuffon(){
	var nrAruncari float64
	var lungime float64
	var nr float64

	lungime = 1
	nrAruncari = 10000
	fmt.Print(" nrAruncari = ")
	fmt.Scan(&nrAruncari)
	fmt.Print(" lungime = ")
	fmt.Scan(&lungime)

	nr = 0
	for i := 1; i <= int(nrAruncari); i++ {
		y := rand.Float64() * float64(100*lungime)
		alfa := rand.Float64() * math.Pi / 2
		var y1 = y + float64(lungime)*math.Sin(alfa)
		b := int(y/lungime) != int(y1/lungime)
		if b {
			nr++
		}
	}
	fmt.Println(2. * nrAruncari / nr)

}

