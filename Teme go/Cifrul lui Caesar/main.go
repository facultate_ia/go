package main
import (
	"fmt"
	"./caesar"
)
func main() {
	var k = 5;
	var text = "secret text";
	fmt.Println("Text:", text);
	var encodedString = caesar.Encode(text, k);
	fmt.Println("Encoded text:", encodedString);
	fmt.Println("Decoded text:", caesar.Decode(encodedString, k));
}
