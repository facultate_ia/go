package main

import (
	"./sudoku"
	"fmt"
)

func main() {
	table := [9][9]int{
		{5, 6, 3, 8, 3, 2, 1, 8, 9},
		{4, 5, 6, 7, 8, 9, 1, 2, 3},
		{7, 8, 9, 1, 2, 3, 4, 5, 6},
		{1, 1, 2, 3, 7, 5, 7, 6, 8},
		{3, 4, 5, 6, 9, 8, 9, 1, 2},
		{6, 7, 8, 4, 1, 2, 3, 4, 5},
		{8, 9, 1, 3, 3, 4, 8, 6, 7},
		{2, 3, 4, 5, 6, 7, 8, 9, 1},
		{5, 6, 7, 4, 9, 1, 2, 3, 5}}

	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			fmt.Print(table[i][j], " ")
		}
		fmt.Println()
	}

	if SudokuVerifier.VerifyConfig(table) == true {
		fmt.Print("Valid!")
	} else {
		fmt.Print("Invalid!")
	}

}