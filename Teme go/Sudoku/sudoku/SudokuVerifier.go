package SudokuVerifier

func VerifyConfig(table [9][9]int) bool {
	var a [3][9]int

	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			x := table[i][j] - 1
			t := 1 << x
			if a[0][i]&t != 0 {
				return false
			}
			a[0][i] |= t
			if a[1][j]&t != 0 {
				return false
			}
			a[1][j] |= t

			k := i/3*3 + j/3
			if a[2][k]&t != 0 {
				return false
			}
			a[2][k] |= t

		}
	}
	return true
}

