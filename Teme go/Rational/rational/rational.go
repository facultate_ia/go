package rational

import (
	"strconv"
	"strings"
)

type Rational struct {
	Numa, Numi int
}

func (r Rational) newInstance (x, y int) Rational{
	r.Numa = x
	r.Numi = y

	return r
}

func (r Rational) getNuma() int{
	return r.Numa
}

func (r Rational) getNumi() int{
	return r.Numi
}



func (r Rational) Add (t Rational) Rational{
	r.Numa = r.Numa * t.Numi + t.Numa * r.Numi

	if r.Numi != r.Numa{
		r.Numi *= t.Numi
	}

	return r
}

func (r Rational) Substract (t Rational) Rational{
	r.Numa = r.Numa * t.Numi - t.Numa * r.Numi

	if r.Numi != r.Numa{
		r.Numi *= t.Numi
	}

	return r

}

func (r Rational) Multiply (t Rational) Rational {

	r.Numa *= t.Numa
	r.Numi *= t.Numi

	return r.simplify(r)
}

func (r Rational) MultiplyInt (t int) Rational {
	r.Numa *= t

	return r
}

func (r Rational) DivideInt (t int) Rational {
	r.Numi *= t

	return r.simplify(r)
}

func (r Rational) SubstractInt (t int) Rational {

	r.Numa -= t * r.Numi
	r = r.simplify(r)

	return r
}

func (r Rational) AddInt (t int) Rational {
	r.Numa += t * r.Numi

	return r
}

func (r Rational) AddNuma (t int) Rational {
	r.Numa += t

	return r
}

func (r Rational) AddNumi (t int) Rational {
	r.Numi += t

	return r
}

func (r Rational) AddNumaAndNumi (t int) Rational {
	r.Numa += t
	r.Numi += t

	return r
}

func (r Rational) SubstractNuma (t int) Rational {
	r.Numa -= t

	return r
}

func (r Rational) SubstractNumi (t int) Rational {
	r.Numi -= t

	return r
}

func (r Rational) SubstractNumaAndNumi (t int) Rational {
	r.Numa -= t
	r.Numi -= t

	return r
}

func (r Rational) IsNull () bool {

	return r.Numa == 0
}

func (r Rational) GetRealValue () float32 {

	return float32(r.Numa) / float32(r.Numi)
}

func (r Rational) GetAbsValue () Rational {

	if r.Numa > 0{
		r.Numa *= (-1)
	}
	if r.Numi > 0{
		r.Numi *= (-1)
	}

	return r
}

func (r Rational) Divide (t Rational) Rational{
	r.Numa *= t.Numi
	r.Numi *= t.Numa

	return r
}

func (r Rational) Pow (t int) Rational{


	if t < 0{
		r = r.Inverse()
		t *= (-1)
	}

	var temp = r
	for i := 1; i < t; i++ {
		r.Numa *= temp.Numa
		r.Numi *= temp.Numi
	}

	return r
}

func (r Rational) Inverse () Rational{
	var aux = r.Numa
	r.Numa = r.Numi
	r.Numi = aux

	return r
}

func (r Rational) BiggerThan(t Rational) bool{
	if r.Numi != t.Numi{
		r.Numa *= t.Numi
		t.Numa *= r.Numi
	}

	return r.Numa > t.Numa
}

func (r Rational) SmallerThan(t Rational) bool{
	if r.Numi != t.Numi{
		r.Numa *= t.Numi
		t.Numa *= r.Numi
	}

	return r.Numa < t.Numa
}

func (r Rational) Equals (t Rational) bool{

	if r.Numi != t.Numi{
		r.Numa *= t.Numi
		t.Numa *= r.Numi
	}

	return r.Numa == t.Numa && r.Numi == t.Numi
}

func (r Rational) GetFromFloat32 (x float32) Rational{
	var y = strconv.FormatFloat(float64(x), 'f', -1, 32)
	var count = len(strings.Split(y, ".")[1])
	var p = float32(1.0)

	for i:=1; i <= count; i++{
		p *= 10
	}

	r.Numa = int(x * p)
	r.Numi = int(p)

	return r
}

func (r Rational) simplify(t Rational) Rational {


	var c = cmmdc(r.Numa, r.Numi)

	r.Numa /= c
	r.Numi /= c

	if r.Numi < 0 {
		r.Numi *= (-1)
		r.Numa *= (-1)
	}
	return r
}

func cmmdc(a, b int) int {
	for a%b != 0 {
		r := a % b
		a = b
		b = r
	}
	return b
}