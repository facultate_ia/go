package main

import "fmt"

func main() {

	var table_sudo = [][]uint8{
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{2, 3, 4, 5, 6, 7, 8, 9, 1},
		{3, 4, 5, 6, 7, 8, 9, 1, 2},
		{4, 5, 6, 7, 8, 9, 1, 2, 3},
		{5, 6, 7, 8, 9, 1, 2, 3, 4},
		{6, 7, 8, 9, 1, 2, 3, 4, 5},
		{7, 8, 9, 1, 2, 3, 4, 5, 6},
		{8, 9, 1, 2, 3, 4, 5, 6, 7},
		{9, 1, 2, 3, 4, 5, 6, 7, 8},
	}

	if check_config(table_sudo) == true {
		fmt.Println("Configuratie corecta!")
	} else {
		fmt.Println("Configuratie incorecta!")
	}
}

func check_config(table [][] uint8) bool {
	var a = [3][9] uint8 {}
	var p uint8
	var k uint8
	for i := 0; i < 9;i++ {
		for j := 0; j < 9; j++ {
			k = uint8(i/3*3 + j/3)
			p = table[i][j] - 1
			if a[0][i] & (1 << p) == 0 {
				a[0][i] = a[0][i] | (1 << p)
			} else {
				return false
			}
			if a[1][j] & (1 << p) == 0 {
				a[1][j] = a[1][j] | (1 << p)
			}else {
				return false
			}
			if a[2][k] & (1 << p) == 0 {
				a[2][j] = a[2][k]
			}else {
				return false
			}
		}
	}
	return true
}