package main

import (
	"fmt"
)

func main() {

	print("Write the number you Want to shift: ")

	var number uint
	fmt.Scan(&number)

	var pBytes uint
	print("Write p: ")
	fmt.Scan(&pBytes)

	print("Write t: ")
	var tBytesShifted uint
	fmt.Scan(&tBytesShifted)

	result := number >> tBytesShifted
	result = result & (1 << pBytes - 1)
	print("Result: ")

	fmt.Print(result)
}

