
import (
    "fmt"
)

var numbers = []int{1, 2, 3, 4, 5}

var allLess10 = every(numbers, func(x int) bool {
        return x &lt; 10
})
//allLess10 is true

var someMore3 = some(numbers, func(x int) bool {
        return x &gt; 3
})
//someMore3 is true

var allOdd = every(numbers, func(x int) bool {
        return x%2 == 1
})
//allOdd is false

fmt.Println(allLess10)
fmt.Println(someMore3)
fmt.Println(allOdd)

func some(arr []int, f func(int) bool) bool {
    for _, v := range arr {
        if f(v) {
            return true
        }
    }
    return false
}

func every(arr []int, f func(int) bool) bool {
    for _, v := range arr {
        if !f(v) {
            return false
        }
    }
    return true
}
Try it in Playground