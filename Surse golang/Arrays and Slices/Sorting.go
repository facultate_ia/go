
import (
    "fmt"
    "sort"
)

var numbers = []int{11, 2, 5, 7, 3}
sort.Ints(numbers)
//numbers is { 2, 3, 5, 7, 11 }

//descending
sort.Sort(sort.Reverse(sort.IntSlice(numbers)))
//numbers is { 11, 7, 5, 3, 2 }

fmt.Println(numbers)
Try it in Playground