
import (
    "fmt"
)

var numbers = []int{1, 2, 3, 4, 5}
var oddItems = filter(numbers, func(v int) bool {
        return v%2 == 1
})
//oddItems is { 1, 3, 5 }

fmt.Println(oddItems)

func filter(arr []int, f func(int) bool) []int {
    result := make([]int, 0)
    for _, v := range arr {
        if f(v) {
            result = append(result, v)
        }
    }
    return result
}
Try it in Playground