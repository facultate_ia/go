
import (
    "fmt"
)

var value = 5
var count = 3
var array = make([]int, count)
for i := 0; i &lt; count; i++ {
    array[i] = value
}
//array is { 5, 5, 5 }

fmt.Println(array)
Try it in Playground