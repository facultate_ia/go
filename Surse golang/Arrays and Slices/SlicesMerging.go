
import (
    "fmt"
)

var firstNumbers = []int{2, 3, 5}
var secondNumbers = []int{7, 11, 13}

var allNumbers = append(firstNumbers, secondNumbers...)
//allNumbers is { 2, 3, 5, 7, 11, 13 }

fmt.Println(allNumbers)
Try it in Playground