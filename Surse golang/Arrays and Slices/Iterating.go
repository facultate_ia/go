
import (
    "fmt"
    "strconv"
)

var numbers = []int{2, 3, 5, 7, 11, 13, 17}
var str = ""
for _, number := range numbers {
    if len(str) &gt; 0 {
        str += "; "
    }
    str += strconv.Itoa(number)
}
//str is "2; 3; 5; 7; 11; 13; 17"

fmt.Println(str)
Try it in Playground