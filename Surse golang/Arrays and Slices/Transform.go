
import (
    "fmt"
)

var numbers = []int{1, 2, 3, 4, 5}
transform(numbers, func(x int) int {
        return x * 3
})
//numbers is { 3, 6, 9, 12, 15 }

fmt.Println(numbers)

func transform(arr []int, f func(int) int) {
    for i := range arr {
        arr[i] = f(arr[i])
    }
}


Try it in Playground