
type stack []int

func (s stack) Push(v int) stack {
    return append(s, v)
}

func (s stack) Pop() (stack, int) {
    l := len(s)
    return s[:l-1], s[l-1]
}

intStack := make(stack, 0)
intStack = intStack.Push(1)
intStack = intStack.Push(3)
intStack = intStack.Push(5)

intStack, first := intStack.Pop()
//first is 5
intStack, second := intStack.Pop()
//second is 3
intStack, third := intStack.Pop()
//third is 1
Try it in Playground