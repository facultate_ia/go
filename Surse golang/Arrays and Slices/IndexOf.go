
import (
    "fmt"
)

var numbers = []int{2, 3, 5, 7, 11, 13, 17}
var contain5 = indexOf(numbers, 5) != -1
//contain5 is true

var index5 = indexOf(numbers, 5)
//index5 is 2

var contain10 = indexOf(numbers, 10) != -1
//contain10 is false

fmt.Println(contain5)
fmt.Println(index5)
fmt.Println(contain10)

func indexOf(arr []int, v int) int {
    for i := range arr {
        if arr[i] == v {
            return i
        }
    }
    return -1
}
Try it in Playground