
import (
    "fmt"
    "strconv"
    "sync"
    "time"
)

var wg sync.WaitGroup

func add(a, b int) int {
    time.Sleep(3000)
    return a + b
}

func startGoroutine() {
    defer wg.Done()
    result := add(3, 5)
    fmt.Println("result: " + strconv.Itoa(result))
}

wg.Add(1) 
go startGoroutine()
wg.Wait()

fmt.Println("main thread")

//output:
//result: 8
//main thread
Try it in Playground