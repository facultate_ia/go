package main

import (
	"fmt"
)

type counter struct {
	Count int
}

func (c *counter) incBy(value int) {
	c.Count += value
}

func (c *counter) incByAmount(value, amount int) {
	c.Count += value * amount
}

func main() {
	c := counter{0}
	c.incBy(1)
	//counter.Count is 1
	fmt.Println(c.Count)

	c.incByAmount(2, 5)
	//counter.Count is 11
	fmt.Println(c.Count)
}
