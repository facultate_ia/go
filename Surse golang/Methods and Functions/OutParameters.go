package main

import (
	"fmt"
)

func getSum(sum *int, n1, n2 int) {
	*sum = n1 + n2
}

func main() {
	var sum = 0
	getSum(&sum, 5, 3)
	//sum is 8

	fmt.Println(sum)
}
