package main

import (
	"fmt"
)

func getFirstLast(ar []int) (int, int) {
	var first = -1
	var last = -1
	if len(ar) > 0 {
		first = ar[0]
		last = ar[len(ar)-1]
	}
	return first, last
}

func main() {
	var ar = []int{2, 3, 5}
	first, last := getFirstLast(ar)
	//first is 2
	//last is 5

	fmt.Println(first)
	fmt.Println(last)
}
