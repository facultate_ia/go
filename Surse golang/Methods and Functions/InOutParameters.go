package main

import (
	"fmt"
)

func swapStrings(s1, s2 *string) {
	*s1, *s2 = *s2, *s1
}

func main() {
	var s1 = "A"
	var s2 = "B"
	swapStrings(&s1, &s2)
	//s1 is "B", s2 is "A"

	fmt.Println(s1)
	fmt.Println(s2)
}