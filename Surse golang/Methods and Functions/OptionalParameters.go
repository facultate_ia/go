package main

import (
	"fmt"
)

func sayGoodby1(message string) {
	if len(message) == 0 {
		fmt.Println("Goodby!")
	} else {
		fmt.Println(message)
	}
}

func sayGoodby2(message ...string) {
	if len(message) == 0 {
		fmt.Println("Goodby!")
	} else {
		fmt.Println(message[0])
	}
}

func main() {
	sayGoodby1("")
	//printed "Goodby!"

	sayGoodby1("see you")
	//printed "see you"

	sayGoodby2()
	//printed "Goodby!"

	sayGoodby2("see you")
	//printed "see you"
}
