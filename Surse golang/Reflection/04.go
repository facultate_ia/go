package main

import (
	"fmt"
	"reflect"
)

type iMac struct {
	Model string
}

func (m iMac) GetModel() string {
	return m.Model
}

func main() {
	iMac := iMac{"2017 Late"}

	t := reflect.TypeOf(iMac)
	kind := t.Kind()

	var isStruct = kind == reflect.Struct
	//isStruct is true
	var isArray = kind == reflect.Array
	//isArray is false
	var isFunc = kind == reflect.Func
	//isFunc is false
	if isFunc {
		var funcParamCount = t.NumIn()
		fmt.Println(funcParamCount)
	}

	fmt.Println(isStruct)
	fmt.Println(isArray)
	fmt.Println(isFunc)
}
