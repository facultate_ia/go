package main

import (
	"fmt"
	"reflect"
)

type macBook struct {
	Model string
}

func (mb macBook) GetModel() string {
	return mb.Model
}

func main() {
	macBook := macBook{"2014 Late"}

	v := reflect.ValueOf(macBook)

	fmt.Println("Number of fields", v.NumField())
	for i := 0; i < v.NumField(); i++ {
		fmt.Printf("Name:%s value:%v\n",
			v.Type().Field(i).Name, v.Field(i))
	}

	fmt.Println("Number of methods", v.NumMethod())
	for i := 0; i < v.NumMethod(); i++ {
		fmt.Printf("Name:%s\n", v.Type().Method(i).Name)
	}
}
