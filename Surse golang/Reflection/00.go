package main

import (
	"fmt"
	"reflect"
)

type sportCar struct {
	Speed int
}

func (c *sportCar) IncreaseSpeed(value int) {
	c.Speed += value
}

func main() {
	car := sportCar{100}
	reflect.ValueOf(&car).
		MethodByName("IncreaseSpeed").
		Call([]reflect.Value{reflect.ValueOf(50)})
	//car.Speed is 150

	fmt.Println(car.Speed)

}
