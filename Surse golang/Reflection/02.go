package main

import (
	"fmt"
	"reflect"
)

//Car is simple struct
type Car struct {
	Model string
}

func main() {
	car := Car{"Airwave"}

	t := reflect.TypeOf(car)
	carValue := reflect.New(t)
	car2 := carValue.Elem().Interface().(Car)
	car2.Model = "Fit"

	fmt.Println(car2)
}
