
import (
    "fmt"
    "io/ioutil"
)

dir := `C:\tmp`
files, err := ioutil.ReadDir(dir)
if err != nil {
    fmt.Println(err)
}

for _, f := range files {
    fmt.Println(f.Name())
}

