
import (
    "fmt"
    "os"
    "path/filepath"
    "syscall"
)

var filePath = `C:\tmp\file.txt`
fi, err := os.Stat(filePath)
if err != nil {
    fmt.Println(err)
    return
}

//file size
fileSize := fi.Size()

//file modification date
var dateChanges = fi.ModTime()

//file creation date (On a Unix system)
//stat := fi.Sys().(*syscall.sta)
//var creationDate = time.Unix(int64(stat.Ctim.Sec), int64(stat.Ctim.Nsec))

//is hidden only file (On Windows)
pointer, _ := syscall.UTF16PtrFromString(filePath)
attributes, _ := syscall.GetFileAttributes(pointer)
isHidden := attributes&amp;syscall.FILE_ATTRIBUTE_HIDDEN != 0

//is read only file (On Windows)
isReadOnly := attributes&amp;syscall.FILE_ATTRIBUTE_READONLY != 0

//file extension
extension := filepath.Ext(filePath)

//file name
fileName := filepath.Base(filePath)

//file name without extension
fileNameOnly := fileName[0 : len(fileName)-len(extension)]

//file directory
fileDir := filepath.Dir(filePath)
