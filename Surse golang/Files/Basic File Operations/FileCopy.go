
import (
    "fmt"
    "io"
    "os"
)

func copyFile(src, dst string) error {
    in, err := os.Open(src)
    if err != nil {
        return err
    }
    defer in.Close()

    out, err := os.Create(dst)
    if err != nil {
        return err
    }
    defer out.Close()

    _, err = io.Copy(out, in)
    return err
}

var filePath = `C:\tmp\file.txt`
var filePathTo = `C:\tmp\file_copy.txt`
err := copyFile(filePath, filePathTo)
if err != nil {
    fmt.Println(err)
} else {
    fmt.Println("copied successfully")
}
