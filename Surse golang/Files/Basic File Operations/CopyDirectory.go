
import (
    "fmt"
    "io"
    "os"
)

path := `C:\tmp\1`
pathCopy := `C:\tmp\1_copy`
err := copyDir(path, pathCopy)
if err != nil {
    fmt.Println(err)
}

func copyFile(src, dst string) (err error) {
    in, err := os.Open(src)
    if err != nil {
        return err
    }
    defer in.Close()

    out, err := os.Create(dst)
    if err != nil {
        return err
    }
    defer out.Close()

    _, err = io.Copy(out, in)
    return err
}

func copyDir(src, dst string) (err error) {

    // get properties of source dir
    srcInfo, err := os.Stat(src)
    if err != nil {
        return err
    }

    // create dest dir
    err = os.MkdirAll(dst, srcInfo.Mode())
    if err != nil {
        return err
    }

    directory, _ := os.Open(src)
    objects, err := directory.Readdir(-1)

    for _, obj := range objects {

        srcPath := src + "/" + obj.Name()
        dstPath := dst + "/" + obj.Name()

        if obj.IsDir() {
            // create sub-directories - recursively
            err = copyDir(srcPath, dstPath)
            if err != nil {
                return err
            }
        } else {
            // perform copy
            err = opyFile(srcPath, dstPath)
            if err != nil {
                return err
            }
        }
    }
    return
}
