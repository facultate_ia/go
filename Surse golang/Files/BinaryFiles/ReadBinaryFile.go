
import (
    "fmt"
    "io/ioutil"
    "os"
)

var filePath = `C:\tmp\file.out`

//first method
bytes, err := ioutil.ReadFile(filePath)
if err != nil {
    fmt.Print(err)
}
fmt.Println(bytes)

//second method
file, err := os.Open(filePath)
if err != nil {
    fmt.Print(err)
}
defer file.Close()
bytes2, err := ioutil.ReadAll(file)
if err != nil {
    fmt.Print(err)
}
fmt.Println(bytes2)
