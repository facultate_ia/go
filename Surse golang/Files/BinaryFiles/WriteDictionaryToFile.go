
import (
    "bytes"
    "encoding/json"
    "io/ioutil"
) 

var dic = map[int]string{1: "one", 2: "two"}
var filePath = `C:\tmp\file.out`

b := new(bytes.Buffer)
e := json.NewEncoder(b)

// Encoding the map
err := e.Encode(dic)
if err != nil {
    panic(err)
}

err = ioutil.WriteFile(filePath, b.Bytes(), 0644)
if err != nil {
    panic(err)
}
