
import (
    "bytes"
    "encoding/json"
    "fmt"
    "io/ioutil"
)

var filePath = `C:\tmp\file.out`
data, _ := ioutil.ReadFile(filePath)

b := new(bytes.Buffer)
b.Write(data)

var dic map[int]string
var d = json.NewDecoder(b)
d.Decode(&amp;dic)

fmt.Println(dic)
