
import (
    "fmt"
    "io/ioutil"
    "os"
)

var filePath = `C:\tmp\file.out`
data := []byte{120, 64, 97}

//first method
err := ioutil.WriteFile(filePath, data, 0644)
if err != nil {
    fmt.Println(err)
}

//second method
outFile, err := os.Create(filePath)
if err != nil {
    fmt.Println(err)
}
n, err := outFile.Write(data)
if err != nil {
    fmt.Println(err)
}
fmt.Println(n, "bytes are written")
