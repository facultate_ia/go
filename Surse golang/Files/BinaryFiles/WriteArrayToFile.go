
import (
    "bytes"
    "encoding/json"
    "io/ioutil"
)

var numbers = []int{1, 2, 3}
var filePath = `C:\tmp\file.out`

b := new(bytes.Buffer)
e := json.NewEncoder(b)

// Encoding the array
err := e.Encode(numbers)
if err != nil {
    panic(err)
}

err = ioutil.WriteFile(filePath, b.Bytes(), 0644)
if err != nil {
    panic(err)
}
