
import (
    "bytes"
    "encoding/json"
    "fmt"
    "io/ioutil"
)

var filePath = `C:\tmp\file.out`
data, _ := ioutil.ReadFile(filePath)

b := new(bytes.Buffer)
b.Write(data)

var numbers []int
var d = json.NewDecoder(b)
d.Decode(&amp;numbers)

fmt.Println(numbers)
