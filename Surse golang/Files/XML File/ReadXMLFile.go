
//XML example:
//&lt;Lines&gt;
//    &lt;Line Id="1"&gt;one&lt;/Line&gt;
//    &lt;Line Id="2"&gt;two&lt;/Line&gt;
//&lt;/Lines&gt;

type Lines struct {
    Lines []Line `xml:"Line"`
}

type Line struct {
    Line string `xml:",innerxml"`
    ID   int    `xml:"Id,attr"`
}

var filePath = `C:\tmp\data.xml`

xmlFile, err := os.Open(filePath)
if err != nil {
    panic(err)
}
defer xmlFile.Close()
bytes, _ := ioutil.ReadAll(xmlFile)

var lines Lines
xml.Unmarshal(bytes, &amp;lines)

for _, line := range lines.Lines {
    fmt.Printf("value: %s, id: %d\n", 
    line.Line, line.ID)
}
//printed:
//value: one, id: 1
//value: two, id: 2
