
import (
    "encoding/xml"
    "io/ioutil"
)

//XML example:
//&lt;Lines&gt;
//    &lt;Line Id="1"&gt;one&lt;/Line&gt;
//    &lt;Line Id="2"&gt;two&lt;/Line&gt;
//&lt;/Lines&gt;

//Lines is ...
type Lines struct {
    Lines []Line `xml:"Line"`
}

//Line is ...
type Line struct {
    Line string `xml:",innerxml"`
    ID   int    `xml:"Id,attr"`
}

var filePath = `C:\tmp\data.xml`

note := &amp;Lines{Lines: []Line{
        Line{"one", 1},
        Line{"two", 2}}}

file, _ := xml.MarshalIndent(note, "", "\t")
ioutil.WriteFile(filePath, file, 0644)
