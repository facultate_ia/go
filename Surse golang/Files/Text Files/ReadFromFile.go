
import (
    "fmt"
    "io/ioutil"
    "os"
)

var filePath = `C:\tmp\file.txt`

//first method
bytes, err := ioutil.ReadFile(filePath)
if err != nil {
    fmt.Print(err)
}
text := string(bytes)
fmt.Println(text)

//second method
file, err := os.Open(filePath)
if err != nil {
    fmt.Print(err)
}
defer file.Close()
bytes2, err := ioutil.ReadAll(file)
if err != nil {
    fmt.Print(err)
}
text2 := string(bytes2)
fmt.Println(text2)
