
import (
    "fmt"
    "os"
)

var filePath = `C:\tmp\file.txt`
lines := []byte("\nLine 3\nLine 4")

outFile, err := os.OpenFile(
    filePath, os.O_APPEND|os.O_WRONLY, 0600)
if err != nil {
    fmt.Println(err)
}
n, err := outFile.Write(lines)
if err != nil {
    fmt.Println(err)
}
fmt.Println(n, "bytes are written")

n, err = outFile.WriteString("\nLine 5")
if err != nil {
    fmt.Println(err)
}
fmt.Println(n, "bytes are written")
