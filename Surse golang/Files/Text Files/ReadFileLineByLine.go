
import (
    "bufio"
    "fmt"
    "os"
)

var filePath = `C:\tmp\file.txt`
file, err := os.Open(filePath)
if err != nil {
    panic(err)
}
defer file.Close()

scanner := bufio.NewScanner(file)
for scanner.Scan() {
    fmt.Println(scanner.Text())
}

if err := scanner.Err(); err != nil {
    fmt.Println(err)
}
