
import (    
    "fmt"
    "io/ioutil"
    "os"
)

var filePath = `C:\tmp\file.txt`
lines := []byte("Line 1\nLine 2")

//first method
err := ioutil.WriteFile(filePath, lines, 0644)
if err != nil {
    fmt.Println(err)
}

//second method
outFile, err := os.Create(filePath)
if err != nil {
    fmt.Println(err)
}
n, err := outFile.Write(lines)
if err != nil {
    fmt.Println(err)
}
fmt.Println(n, "bytes are written")

n, err = outFile.WriteString("\nLine 3")
if err != nil {
    fmt.Println(err)
}
fmt.Println(n, "bytes are written")

