package main

import (
	"fmt"
)

//There are no classes in golang
type shape struct {
	LineCount int
	Name      string
}

func main() {
	var square = shape{4, "Square"}
	squareCopy := square

	square.Name = "test"

	fmt.Println(square)
	fmt.Println(squareCopy)
}
