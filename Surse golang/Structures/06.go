package main

import (
	"fmt"
)

type citizen struct {
	Name    string
	Country string
}

func newCitizen(param ...string) citizen {
	switch len(param) {
	case 0:
		return citizen{"unknown", "unknown"}
	case 1:
		return citizen{param[0], "unknown"}
	default:
		return citizen{param[0], param[1]}
	}
}

func main() {
	var man1 = newCitizen()
	//man1.Name is "unknown"
	//man1.Country is "unknown"

	var man2 = newCitizen("Vladimir")
	//man2.Name is "Vladimir"
	//man2.Country is "unknown"

	var man3 = newCitizen("Vladimir", "Brazil")
	//man3.Name is "Vladimir"
	//man3.Country is "Brazil"

	fmt.Println(man1)
	fmt.Println(man2)
	fmt.Println(man3)
}
