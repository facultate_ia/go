package main

import (
	"fmt"
)

type point struct {
	//In Golang there are no properties
	x, y int
}

func main() {
	var p = point{}
	//x and y is 0 (before assigning)
	p.x = 3
	p.y = 7

	fmt.Println(p)
}
