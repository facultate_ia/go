package main

import (
	"fmt"
	"math"
)

//In Golang there are no properties
type circle struct {
	Radius float64
}

func (c circle) GetArea() float64 {
	return 2 * math.Pi * c.Radius
}

func main() {
	var circle2 = circle{2.0}
	var area = circle2.GetArea()
	//area is 12.56

	fmt.Println(area)
}
