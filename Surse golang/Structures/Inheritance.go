package main

import (
	"fmt"
)

type shape interface {
	GetArea() int
}

type square struct {
	SideLength int
}

func (s square) GetArea() int {
	return s.SideLength * s.SideLength
}

func main() {
	square5 := square{5}
	area := square5.GetArea()
	//area is 25

	fmt.Println(area)
}
