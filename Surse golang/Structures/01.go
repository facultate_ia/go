package main

import (
	"fmt"
)

//There are no classes in golang

//Calendar is simple struct
type Calendar struct{}

func (c Calendar) months() int {
	return 12
}

func main() {
	var calendar = Calendar{}
	var months = calendar.months()

	fmt.Println(months)
}
