package main

import (
	"fmt"
)

//There are no classes in golang
type game struct {
	// public field
	Name string
	Year int
}

func main() {
	var game = game{"Herios", 2005}

	fmt.Println(game)
}
