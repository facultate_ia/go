package main

import (
	"fmt"
)

type filmsList struct{}

func newFilmsList() filmsList {
	//some long operation
	return filmsList{}
}

type mediaPlayer struct {
	//In Golang there are no properties
	films filmsList
}

func (mp *mediaPlayer) GetFilmsList() filmsList {
	mp.films = newFilmsList()
	return mp.films
}

func main() {
	var player = mediaPlayer{}
	//filmsList field not yet been initialized
	//It will be created after call GetFilmsList() function
	var filmList = player.GetFilmsList()

	fmt.Println(filmList)
}
