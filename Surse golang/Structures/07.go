package main

import (
	"fmt"
)

type man struct {
	Name string
}

func makeMan(name string) man {
	return man{name}
}

func main() {
	man := makeMan("Alex")
	//man.Name is "Alex"

	fmt.Println(man.Name)
}
