package main

import (
	"fmt"
	"math"
)

//In Golang there are no properties
type square struct {
	side float64
}

func (s square) GetArea() float64 {
	return s.side * s.side
}

func (s *square) SetArea(area float64) {
	s.side = math.Sqrt(area)
}

func main() {
	var square2 = square{2.0}
	var area = square2.GetArea()
	//square.area is 4.0
	fmt.Println(area)

	square2.SetArea(9)
	//square2.side is 3.0
	fmt.Println(square2.side)
}
