package main

import (
	"fmt"
)

//There are no classes in golang
type game struct {
	Name      string
	Callbacks []func(game game)
}

func (g game) Start() {
	for _, callback1 := range g.Callbacks {
		callback1(g)
	}
}

func (g *game) AddListener(s *statistic) {
	g.Callbacks = append(g.Callbacks, s.GameStarted)
}

type statistic struct {
	StartCount int
	LastGame   string
}

func (s *statistic) GameStarted(game game) {
	s.LastGame = game.Name
	s.StartCount++
}

func main() {
	var statistic = statistic{}
	var heroes = game{Name: "Heroes"}
	var doom = game{Name: "Doom"}

	//subscribe to events
	heroes.AddListener(&statistic)
	doom.AddListener(&statistic)

	doom.Start()
	heroes.Start()
	//statistic.LastGame is "Heroes"
	//statistic.StartsCount is 2

	fmt.Println(statistic.LastGame)
	fmt.Println(statistic.StartCount)
}
