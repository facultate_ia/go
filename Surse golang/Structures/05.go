package main

import (
	"fmt"
)

//There are no classes in golang
type man struct {
	Name string
}

func main() {
	man := man{}

	fmt.Println(man)
}
