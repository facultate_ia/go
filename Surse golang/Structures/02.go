package main

import (
	"fmt"
)

type man struct {
	Name    string
	Country string
}

func newMan(name string) man {
	return man{
		Name: name,
	}
}

func newManWithCountry(name, country string) man {
	man := newMan(name)
	man.Country = country
	return man
}

func main() {
	man := newManWithCountry("Vladimir", "Russia")

	fmt.Println(man)
}
