package main

import (
	"fmt"
)

type shape struct {
	lineCount int
}

type square struct {
	sideLength int
	shape
}

func main() {

	square2 := square{2, shape{4}}
	//square.lineCount is 4

	fmt.Println(square2.lineCount)
}
