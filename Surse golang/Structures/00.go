package main

import (
	"fmt"
	"reflect"
)

//There are no classes in golang
type car struct{}

func main() {
	var car1 = car{}
	var car2 = car{}
	var car3 = car1

	var equal1 = car1 == car2
	//equal1 is True

	var equal2 = car1 == car3
	//equal2 is True

	var equal3 = reflect.DeepEqual(car1, car2)
	//equal3 is True

	var equal4 = &car1 == &car2
	//equal4 is False

	fmt.Println(equal1)
	fmt.Println(equal2)
	fmt.Println(equal3)
	fmt.Println(equal4)
}
