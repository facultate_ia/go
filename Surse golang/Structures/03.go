package main

import (
	"fmt"
)

type man struct {
    Name string
}

type employee struct {
    Position string
    man
}

func main() {
	empl := employee{"booker", man{"Max"}}

	fmt.Println(empl.Name)
}
