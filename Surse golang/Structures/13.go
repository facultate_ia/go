package main

import (
	"fmt"
)

//In Golang there are no properties
//It is possible to make a struct to be
//read-only outside of package by making it's
//members non-exported and providing readers
type filmList struct {
	count int
}

func (fl filmList) GetCount() int {
	return fl.count
}

func main() {
	var films = filmList{10}
	var count = films.GetCount()
	//count is 10

	fmt.Println(count)
}
