package main

import (
	"fmt"
)

//There are no classes in golang
type someStruct struct {
	intValue int

	nestedStruct struct {
		strValue string
	}
}

type someStruct2 struct {
	intValue int
	nestedStruct2
}

type nestedStruct2 struct {
	strValue string
}

func main() {
	var someStruct = someStruct{
		intValue: 5,
	}
	var nestedStruct = someStruct.nestedStruct
	nestedStruct.strValue = "test"

	var someStruct2 = someStruct2{
		intValue:      5,
		nestedStruct2: nestedStruct2{"test"},
	}

	fmt.Println(nestedStruct)
	fmt.Println(someStruct2)
}
