package main

import (
	"fmt"
)

type man struct {
	Name string
}

func newMan() man {
	return man{"unknown"}
}

func main() {
	man := newMan()
	//man.Name is "unknown"

	fmt.Println(man.Name)
}
