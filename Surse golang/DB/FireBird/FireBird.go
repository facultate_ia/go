
//go get github.com/nakagami/firebirdsql

import (
    "database/sql"
    "fmt"

    _ "github.com/nakagami/firebirdsql"
)

// Connect to the database
conStr := "UserName:Password@HostName/databasePath/databaseName.fdb"
conn, err := sql.Open("firebirdsql", conStr)
defer conn.Close()
if err != nil {
    fmt.Println(err)
    return
}

// SQL query execution
statement, err := conn.Prepare(`
    SELECT Name
    FROM country`)
if err != nil {
    fmt.Println(err)
    return
}

// Get the data
result := statement.QueryRow()
var name string
result.Scan(&amp;name)
fmt.Println(name)
