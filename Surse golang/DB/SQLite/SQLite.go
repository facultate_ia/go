
//go get github.com/go-sql-driver/mysql

import (
    "database/sql"
    "fmt"

    _ "github.com/mattn/go-sqlite3"
)

// Connect to the database
conn, err := sql.Open("sqlite3", "/FilePath/DatabaseName.db")
if err != nil {
    fmt.Println(err)
    return
}

// SQL query execution
result := conn.QueryRow(`
    SELECT Name
    FROM country`)

var name string
result.Scan(&amp;name)
fmt.Println(name)

// Close the connection
conn.Close()
