
//go get gopkg.in/goracle.v2

import (
    "database/sql"
    "fmt"

    _ "gopkg.in/goracle.v2"
)

// Connect to the database
conStr := "UserName/Password@HostName:Port/sid"
conn, err := sql.Open("goracle", conStr)
defer conn.Close()
if err != nil {
    fmt.Println(err)
    return
}

// SQL query execution
statement, err := conn.Prepare(`
    SELECT Name
    FROM country`)
if err != nil {
    fmt.Println(err)
    return
}

// Get the data
result := statement.QueryRow()
var name string
result.Scan(&amp;name)
fmt.Println(name)
