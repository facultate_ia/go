
//go get github.com/lib/pq

import (
    "database/sql"
    "fmt"

    _ "github.com/lib/pq"
)

// Connect to the database
conStr := "user=UserName password=Password dbname=DatabaseName sslmode=disable"
conn, err := sql.Open("postgres", conStr)
if err != nil {
    fmt.Println(err)
    return
}

// SQL query execution
statement, err := conn.Prepare(`
    SELECT Name
    FROM country`)
if err != nil {
    fmt.Println(err)
    return
}

// Get the data
result := statement.QueryRow()
var name string
result.Scan(&amp;name)
fmt.Println(name)

// Close the connection
conn.Close()
