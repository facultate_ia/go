
//go get github.com/denisenkom/go-mssqldb

import (
    "database/sql"
    "fmt"

    _ "github.com/denisenkom/go-mssqldb"
)

// Connect to the database
conn, err := sql.Open("mssql", "server=HostName;user id=UserName;password=Password;")
if err != nil {
    fmt.Println(err)
    return
}

// SQL query execution
statement, _ := conn.Prepare(`
    SELECT Name
    FROM country`)

// Get the data
result := statement.QueryRow()
var name string
result.Scan(&amp;name)
fmt.Println(name)

// Close the connection
conn.Close()
