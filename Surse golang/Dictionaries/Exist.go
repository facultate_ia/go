
import (
    "fmt"
)

var dic = map[int]string{
    1: "one", 2: "",
}

if value, exists1 := dic[1]; exists1 {
    fmt.Println(value)
    //exists1 is true
}

_, exists2 := dic[2]
//exists2 is true

_, exists3 := dic[3]
//exists3 is false

fmt.Println(exists2)
fmt.Println(exists3)
Try it in Playground