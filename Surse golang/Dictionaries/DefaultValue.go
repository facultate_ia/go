
import (
    "fmt"
    "strconv"
)

var dic1 = map[int]string{1: "one"}

var strValue = dic1[2]
//strValue is ""

var dic2 = map[string]int{"one": 1}

var intValue = dic2["two"]
//intValue is 0

fmt.Println("\"" + strValue + "\"")
fmt.Println("\"" + strconv.Itoa(intValue) + "\"")
Try it in Playground