
import (
    "fmt"
    "strings"
)

var dic = map[int]string{
    1: "one", 2: "two",
}

var upperDic = map[int]string{}
for k, v := range dic {
    upperDic[k] = strings.ToUpper(v)
}
//upperDic is {1:ONE, 3:TWO}

fmt.Println(upperDic)
Try it in Playground