
import (
    "fmt"
)

var d = map[int]string{
    1: "one",
    2: "two",
}

var one = d[1]
// one is "one"

var two = d[2]
// two is "two"

var three = d[3]
// three is ""

fmt.Println(one)
fmt.Println(two)
fmt.Println(three)
Try it in Playground