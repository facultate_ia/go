
import (
    "fmt"
)

//Empty dictionary
var d1 = map[int]string{}

//init with some data
var d2 = map[int]string{
    1: "one",
    2: "two",
}

fmt.Println(d1)
fmt.Println(d2)
Try it in Playground