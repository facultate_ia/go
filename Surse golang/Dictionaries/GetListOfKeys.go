
import (
    "fmt"
)

var dic = map[int]string{
    3: "three", 1: "one", 2: "two"}

var keys []int
for k := range dic {
    keys = append(keys, k)
}
//keys is {3, 1, 2}

fmt.Println(keys)
Try it in Playground