
import (
    "fmt"
)

var dic = map[int]string{
    1: "one", 2: "two", 3: "three",
}

var oddDic = map[int]string{}
for k, v := range dic {
    if k%2 == 1 {
        oddDic[k] = v
    }
}
//oddDic is {1:one, 3:three}

fmt.Println(oddDic)
Try it in Playground