
import (
    "fmt"
    "strconv"
)

var dic = map[int]string{
    1: "one", 2: "two",
}

var str = ""
for k, v := range dic {
    if len(str) &gt; 0 {
        str += ", "
    }
    str += "{ " + strconv.Itoa(k) +
        ", \"" + v + "\" }"
}
//str is "{ 1, "one" }, { 2, "two" }"

fmt.Println(str)
Try it in Playground