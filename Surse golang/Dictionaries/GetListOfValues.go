
import (
    "fmt"
)

var dic = map[int]string{
    3: "three", 1: "one", 2: "two"}

var values []string
for _, v := range dic {
    values = append(values, v)
}
//values is {"three", "one", "two"}

fmt.Println(values)
Try it in Playground