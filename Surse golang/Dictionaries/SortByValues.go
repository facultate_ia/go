
import (
    "fmt"
    "sort"
)

var dic = map[int]string{3: "B", 1: "C", 2: "A"}

type Pair struct {
    Key   int
    Value string
}

var pairs []Pair
for k, v := range dic {
    pairs = append(pairs, Pair{k, v})
}

sort.Slice(pairs, func(i, j int) bool {
        return pairs[i].Value &lt; pairs[j].Value
})

for _, pair := range pairs {
    fmt.Printf("%d: \"%s\"\n", pair.Key, pair.Value)
}
//printed:
//  2: "A"
//  3: "B"
//  1: "C"
Try it in Playground