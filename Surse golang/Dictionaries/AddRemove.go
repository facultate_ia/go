
import (
    "fmt"
)

//Empty dictionary
var dic = map[int]string{}
dic[1] = "one"
dic[2] = "two"
dic[3] = "tree"
//dic is {1: "one", 2: "two", 3: "tree"}
fmt.Println(dic)

dic[3] = "three"
//dic is {1: "one", 2: "two", 3: "three"}
fmt.Println(dic)

delete(dic, 3)
//dic is {1: "one", 2: "two"}
fmt.Println(dic)

dic = map[int]string{}
//dic is {}
fmt.Println(dic)
Try it in Playground