
import (
    "fmt"
    "sort"
)

var dic = map[int]string{
    3: "three", 1: "one", 2: "two"}

var keys []int
for k := range dic {
    keys = append(keys, k)
}
sort.Ints(keys)

for _, k := range keys {
    fmt.Printf("%d: \"%s\"\n", k, dic[k])
}
Try it in Playground