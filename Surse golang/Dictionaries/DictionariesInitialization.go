package main

import (
	"fmt"
)

func main() {
	//Dictionary<String, String>
	var languages = map[string]string{
		"ru": "russian",
		"en": "english",
	}

	//Dictionary<Int, String>
	var numbers = map[int]string{
		1: "one", 2: "two", 3: "three",
	}

	type employee struct {
		FirstName string
		LastName  string
	}

	//Dictionary<Int, Employee>
	var employees = map[int]employee{
		1: employee{"Pavlov", "Anton"},
		2: employee{"Kirienko", "Elena"},
	}

	fmt.Println(languages)
	fmt.Println(numbers)
	fmt.Println(employees)
}
