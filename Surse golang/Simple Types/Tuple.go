package main

import (
	"fmt"
)

//In Golang there are no tuple type
type pair struct {
	s, n interface{}
}

func main() {
	var one = pair{"one", 1}
	var number = one.n
	//number is 1
	var str = one.s
	//str is "one"

	one.n = 2000

	fmt.Println(number)
	fmt.Println(str)
}
