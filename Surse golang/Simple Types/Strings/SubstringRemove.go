package main

import (
	"fmt"
)

func main() {
	var dataString = "Substring removing!"

	//there is no "remove" method
	dataString = dataString[0:9] + dataString[18:]
	//dataString is "Substring!"

	dataString = dataString[3:]
	//dataString is "string!"

	fmt.Println(dataString)
}