package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	//to int
	var strNumber = "42"
	//the first method
	number1, err := strconv.Atoi(strNumber)

	//the second method
	number2, err := strconv.ParseInt(strNumber, 10, 16)

	//to Double and Float
	//the first method
	var strPi = "3.14"
	pi, err := strconv.ParseFloat(strPi, 64)

	//the second method
	var strHalf = "0,5"
	half1, err := strconv.ParseFloat(strHalf, 32)
	//half1 is 0
	if err != nil {
		fmt.Println(err)
	}
	strHalf = strings.Replace(strHalf, ",", ".", 1)
	half2, err := strconv.ParseFloat(strHalf, 32)
	//half2 is 0.5

	fmt.Println(number1)
	fmt.Println(number2)
	fmt.Println(pi)
	fmt.Println(half1)
	fmt.Println(half2)
}
