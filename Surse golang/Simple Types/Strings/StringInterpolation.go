package main

import (
	"fmt"
)

func main() {
	var fontSize = 14
	var fontFamile = "Arial"
	var style = fmt.Sprintf("font-size: %v;font-family: %v",
		fontSize, fontFamile)
	//style is "font-size: 14;font-family: Arial"

	fmt.Println(style)
}
