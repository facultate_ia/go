package main

import (
	"fmt"
)

func main() {
	// \t Insert a tab.
	// \b Insert a backspace.
	// \n Insert a newline.
	// \r Insert a carriage return.
	// ' Insert a single quote.
	// \" Insert a double quote.
	// \\ Insert a backslash character.

	var str = "She said \"Hello!\" to me."
	//str is "She said "Hello!" to me."

	fmt.Println(str)
}
