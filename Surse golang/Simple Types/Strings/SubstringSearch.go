package main

import (
	"fmt"
	"strings"
)

func main() {
	var dataString = "Substring search"
	if strings.Contains(dataString, "string") {
		fmt.Println("contain \"string\"")
	}
	if strings.HasPrefix(dataString, "Sub") {
		fmt.Println("starts with \"Sub\"")
	}
	if strings.HasSuffix(dataString, "search") {
		fmt.Println("ends with \"search\"")
	}
}
