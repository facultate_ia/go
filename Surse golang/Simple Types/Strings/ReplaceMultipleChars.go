package main

import (
	"fmt"
	"strings"
)

func main() {
	var replacer = strings.NewReplacer(
		"[", "", "]", "", "=", "", "/", "")
	var str = "1-/[=2=/]-3"
	str = replacer.Replace(str)
	//str is "1-2-3"

	fmt.Println(str)
}
