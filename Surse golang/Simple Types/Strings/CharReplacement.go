package main

import (
	"fmt"
	"strings"
)

func main() {
	var str = "1-3-2"
	var arrStr = strings.Split(str, "")
	arrStr[2] = "2"
	arrStr[4] = "3"
	str = strings.Join(arrStr, "")
	//str is "1-2-3"

	fmt.Println(str)
}
