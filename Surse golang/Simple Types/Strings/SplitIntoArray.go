package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var strData = "1981|Kim Victorya|engineer"
	var arrData = strings.Split(strData, "|")
	year, err := strconv.Atoi(arrData[0])
	_ = err
	//year is 1981
	var name = arrData[1]
	//name is "Kim Victorya"
	var position = arrData[2]
	//position is "engineer"

	fmt.Println(year)
	//fmt.Println(err)
	fmt.Println(name)
	fmt.Println(position)
}
