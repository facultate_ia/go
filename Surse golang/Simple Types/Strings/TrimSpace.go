package main

import (
	"fmt"
	"strings"
)

func main() {
	var str = "  Spaces  "

	var trimStr = strings.TrimSpace(str)
	//trimStr is "Spaces"

	fmt.Println(trimStr)
}
