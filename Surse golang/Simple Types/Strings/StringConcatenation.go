package main

import (
	"fmt"
	"strings"
)

func main() {
	var s1 = "three"
	var s2 = "two"
	var s3 = "one"

	//first method
	var sGo1 = s1 + ", " + s2 + ", "
	sGo1 += s3
	sGo1 = strings.Join([]string{sGo1, ", ", "go!"}, "")
	//sGo1 is "three, two, one, go!"

	//second method (efficient way)
	var b strings.Builder
	b.WriteString(s1)
	b.WriteString(", ")
	b.WriteString(s2)
	b.WriteString(", ")
	b.WriteString(s3)
	b.WriteString(", go!")
	var sGo2 = b.String()
	//sGo2 is "three, two, one, go!"

	fmt.Println(sGo1)
	fmt.Println(sGo2)
}
