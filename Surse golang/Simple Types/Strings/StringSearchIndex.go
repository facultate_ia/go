package main

import (
	"fmt"
	"strings"
)

func main() {
	var dataString = "Substring index"
	var index = strings.Index(dataString, "string")
	// index is 3

	fmt.Println(index)
}
