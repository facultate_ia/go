package main

import (
	"fmt"
	"strings"
)

func main() {
	var str = "Lower and upper"

	var lower = strings.ToLower(str)
	//lower is "lower and upper"

	var upper = strings.ToUpper(str)
	//upper is "LOWER AND UPPER"

	fmt.Println(lower)
	fmt.Println(upper)
}
