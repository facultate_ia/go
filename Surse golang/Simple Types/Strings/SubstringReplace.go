package main

import (
	"fmt"
	"strings"
)

func main() {
	var startString = "3, 2, 1, go!"
	startString = strings.
		Replace(startString, "1", "one", 1)
	startString = strings.
		Replace(startString, "2", "two", 1)
	startString = strings.
		Replace(startString, "3", "three", 1)
	//startString = "three, two, one, go!"

	//replace all
	var oneString = "1 1 1"
	oneString = strings.
		Replace(oneString, "1", "one", -1)
	//oneString is "one one one"

	fmt.Println(startString)
	fmt.Println(oneString)
}
