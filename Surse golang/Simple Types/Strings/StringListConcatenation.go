package main

import (
	"fmt"
	"strings"
)

func main() {
	var numbers = []string{"one", "two", "three"}
	var numberList = strings.Join(numbers, "; ")
	//numberList is "one; two; three"

	fmt.Println(numberList)
}