package main

import "fmt"

func reverse(word string) string {
	//Characters count
	var charCount = len(word)
	var result = ""
	for i := charCount - 1; i >= 0; i-- {
		result += string(word[i])
	}
	return result
}

func main() {
	var stringReverse = reverse("string")
	//stringReverse = "gnirts"

	fmt.Println(stringReverse)
}
