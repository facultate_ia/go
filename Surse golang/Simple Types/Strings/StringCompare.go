package main

import (
	"fmt"
	"strings"
)

func main() {
	var first = "A"
	var second = "B"
	var third = "A"

	var areEqual1 = first == second
	//areEqual1 is false

	var areNotEqual = first != second
	//areNotEqual is true

	var areEqual2 = strings.Compare(first, third) == 0
	//areEqual2 is true

	fmt.Println(areEqual1)
	fmt.Println(areNotEqual)
	fmt.Println(areEqual2)
}
