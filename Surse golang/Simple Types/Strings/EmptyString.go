package main

import "fmt"

func main() {
	//Empty strings
	var emptyString = ""
	var anotherEmptyString = ""

	if emptyString == "" {
		fmt.Println("string is empty")
	}

	if len(anotherEmptyString) == 0 {
		fmt.Println("string is empty")
	}
}
