
var i = 50
//pointer to int value
var pInt = &amp;i

var d = 3.14
//pointer to double
var pDouble = &amp;d

//address for the pointer
var adr = new(int)
*adr = 0xB8000000
