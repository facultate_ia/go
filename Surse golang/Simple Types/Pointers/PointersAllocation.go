package main

import (
	"fmt"
)

func main() {
	//pointer to int value
	var pInt = new(int)
	*pInt = 50

	//pointer to double
	var pDouble = new(float64)
	var d = 3.14
	*pDouble = d

	fmt.Println(*pInt)
	fmt.Println(*pDouble)
}
