package main

import (
	"fmt"
)

func main() {
	var sun = "sun"
	var pSun = &sun

	var pStar = new(string)
	*pStar = "star"

	fmt.Println(*pSun)
	fmt.Println(*pStar)
}
