package main

import (
	"fmt"
)

//Point is ...
type Point struct {
	x int32
	y int32
}

func main() {
	var p1 = new(Point)
	//p1.x and p1.y is 0
	(*p1).x = 5
	(*p1).y = 7

	var point = Point{1, 2}
	var p2 = &point

	fmt.Println((*p1).x)
	fmt.Println((*p2).y)
}
