package main

import (
	"fmt"
)

func main() {
	var str = "ABC"
	var charA = str[0]
	//charA is 65
	var charB = rune(str[1])
	//charB is 66
	var charC = string(str[2])
	//charC is 'C'

	var charList = ""
	for _, c := range str {
		charList += string(c) + ";"
	}
	//charList is "A;B;C;"

	fmt.Println(charA)
	fmt.Println(charB)
	fmt.Println(charC)
	fmt.Println(charList)
}
