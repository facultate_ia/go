package main

import (
	"fmt"
)

func main() {
	// \' for a single quote.
	var c = '\''

	// " (for rune) or \" (for string) for a double quote.
	c = '"'

	// \\ for a backslash.
	c = '\\'

	// \a for an alert character.
	c = '\a'

	// \b for a backspace.
	c = '\b'

	// \n for a new line.
	c = '\n'

	// \r for a carriage return.
	c = '\r'

	// \t for a horizontal tab.
	c = '\t'

	// \v for a vertical tab.
	c = '\v'

	// \x for a unicode character hex value.
	c = '\xA9' //Copyright Symbol

	fmt.Println("\"" + string(c) + "\"")
}
