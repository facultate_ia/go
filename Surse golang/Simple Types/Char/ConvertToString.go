package main

import (
	"fmt"
)

func main() {
	var charA = 'A'
	var stringA = string(charA)
	//stringA is "A"

	var str = "character " + string(charA)
	//str is "character A"

	fmt.Println(stringA)
	fmt.Println(str)
}
