package main

import (
	"fmt"
)

func main() {
	var charA = 'A'
	//charA is 65

	var charB = string(charA + 1)
	//charB is 'B'

	fmt.Println(charA)
	fmt.Println(charB)
}
