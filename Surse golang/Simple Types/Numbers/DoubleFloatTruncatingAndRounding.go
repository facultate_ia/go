package main

import (
	"fmt"
	"math"
)

func main() {
	var pi = 3.1415
	var piRound1 = math.Round(pi*1000) / 1000
	//piRound1 is 3.142

	var piTrunc = math.Floor(pi*1000) / 1000
	//piTrunc is 3.141

	var piCeil = math.Ceil(pi*100) / 100
	//piCeil is 3.15

	fmt.Println(piRound1)
	fmt.Println(piTrunc)
	fmt.Println(piCeil)
}
