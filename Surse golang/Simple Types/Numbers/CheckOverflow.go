package main

import (
	"fmt"
)

func main() {
	//no range check
	var b1 = 200
	var b2 = 100
	var b3 = byte(b1 + b2)
	//b3 is 44 (300 - 256)

	fmt.Println(b3)
}
