package main

import (
	"fmt"
	"strconv"
)

func main() {
	//decimal number system
	var decimal = int64(42)

	//octal number system
	var octal = 042
	//octal is 34

	//hexadecimal number system
	var hexadecimal = 0x42
	//hexadecimal is 66

	//42 to decimal string
	var sDecimal = strconv.FormatInt(decimal, 10)
	//sDecimal is "42"

	//42 to octal string
	var sOctal = strconv.FormatInt(decimal, 8)
	//sOctal is "52"

	//42 to hexadecimal string
	var sHexadecimal = strconv.FormatInt(decimal, 16)
	//sHexadecimal is "2a"

	//42 to binary string
	var sBinary = strconv.FormatInt(decimal, 2)
	//sBinary is "101010"

	fmt.Println(octal)
	fmt.Println(hexadecimal)
	fmt.Println(sDecimal)
	fmt.Println(sOctal)
	fmt.Println(sHexadecimal)
	fmt.Println(sBinary)
}
