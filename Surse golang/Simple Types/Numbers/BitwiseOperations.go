package main

import (
	"fmt"
)

func main() {
	var a = 5 //0101
	var b = 6 //0110

	//And
	var c1 = a & b
	//c1 is 4 (0100)

	//Or
	var c2 = a | b
	//c2 is 7 (0111)

	//Xor
	var c3 = a ^ b
	//c3 is 3 (0011)

	//shift right
	var c4 = a >> 1
	//c4 is 2 (0010)

	//shift left
	var c5 = b << 1
	//c5 is 12 (1100)

	fmt.Println("c1 =", c1)
	fmt.Println("c2 =", c2)
	fmt.Println("c3 =", c3)
	fmt.Println("c4 =", c4)
	fmt.Println("c5 =", c5)
}
