package main

import (
	"fmt"
	"math"
)

func main() {
	var d1 = 8.5 + 2.4 // d1 is 10.9
	var d2 = 8.5 - 2.4 // d2 is 6.1
	var d3 = 8.5 * 2   // d3 is 17
	var d4 = 8.5 / 2   // d4 is 4.25
	// mod
	var d5 = math.Mod(7.5, 2)  // d5 is 1.5
	var d6 = math.Mod(-7.5, 2) // d6 is -1.5
	// div
	var f = 7.5
	var d7 = int(f) / 2 // d7 is 3
	var d8 = -d7        // d8 is -3.
	var d9 = 3.5
	d9++                     // d9 is 4.5
	d9--                     // d9 is 3.5
	d9 += 2                  // d9 is 5.5
	d9 -= 2                  // d9 is 3.5
	d10, d9 := d9, d9+1      // d10 is 3.5 d9 is 4.5
	d11, d9 := d9+1, d9+1    // d11 is 5.5 d9 is 5.5
	var d12 = math.Abs(-5.5) // d12 is 5.5

	fmt.Println("d1 =", d1)
	fmt.Println("d2 =", d2)
	fmt.Println("d3 =", d3)
	fmt.Println("d4 =", d4)
	fmt.Println("d5 =", d5)
	fmt.Println("d6 =", d6)
	fmt.Println("d7 =", d7)
	fmt.Println("d8 =", d8)
	fmt.Println("d9 =", d9)
	fmt.Println("d10 =", d10)
	fmt.Println("d11 =", d11)
	fmt.Println("d12 =", d12)
}
