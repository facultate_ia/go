package main

import (
	"fmt"
	"strconv"
)

func main() {
	var number = 42
	var s1 = strconv.Itoa(number)
	//s1 is "42"

	var s2 = strconv.FormatInt(int64(number), 10)
	//s2 id "42"

	var s3 = fmt.Sprintf("%d", number)
	//s3 id "42"

	var s4 = fmt.Sprintf("%03d", number)
	//s4 is "042"

	fmt.Println(s1)
	fmt.Println(s2)
	fmt.Println(s3)
	fmt.Println(s4)
}
