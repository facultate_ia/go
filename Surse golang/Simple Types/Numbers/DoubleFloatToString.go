package main

//install packages:
//go get golang.org/x/text/language
//go get golang.org/x/text/message

import (
	"fmt"
	"strconv"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func main() {
	var exp = 2.718281828

	var s1 = strconv.FormatFloat(exp, 'f', 6, 64)
	//s1 is 2.718281828

	var s2 = fmt.Sprintf("%f.3", exp)
	//s2 is 2.718

	var s3 = fmt.Sprintf("%.2e", exp/100)
	//s3 is 2.72E-002

	p := message.NewPrinter(language.Russian)
	var s4 = p.Sprintf("%.2f", exp*1000000)
	//s4 is 2 718 281,83

	fmt.Println(s1)
	fmt.Println(s2)
	fmt.Println(s3)
	fmt.Println(s4)
}
