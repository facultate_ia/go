package main

import (
	"fmt"
	"strconv"
)

func main() {
	var strNumber = "42"
	number1, err := strconv.Atoi(strNumber)
	_ = err

	//the second method
	number2, err := strconv.ParseInt(strNumber, 10, 16)

	fmt.Println(number1)
	fmt.Println(number2)
}
