package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	//the first method
	var strPi = "3.14"
	piFloat, err := strconv.ParseFloat(strPi, 64)
	_ = err
	//piFloat is 3.14 (float64)

	var strExp = "2.71828"
	exp, err := strconv.ParseFloat(strExp, 32)
	//exp is 2.718280076980591 (float32)

	//the third method
	var strHalf = "0,5"
	strHalf = strings.Replace(strHalf, ",", ".", 1)
	half, err := strconv.ParseFloat(strHalf, 32)
	//half is 0.5 (float32)

	fmt.Println(piFloat)
	fmt.Println(exp)
	fmt.Println(half)
}
