package main

import (
	"fmt"
)

func main() {
	var d1 = 8 + 2  //d1 is 10
	var d2 = 8 - 2  //d2 is 6
	var d3 = 8 * 2  //d3 is 16
	var d4 = 8 / 2  //d4 is 4
	var d5 = 5 % 2  //d5 is 1
	var d6 = -5 % 2 //d6 is -1
	var d7 = 1
	d7++                 //d7 is 2
	d7--                 //d7 is 1
	d7, d8 := d7+1, d7   //d8 is 1, d7 is 2
	d7, d9 := d7+1, d7+1 //d9 is 3, d7 is 3

	fmt.Println("d1 =", d1)
	fmt.Println("d2 =", d2)
	fmt.Println("d3 =", d3)
	fmt.Println("d4 =", d4)
	fmt.Println("d5 =", d5)
	fmt.Println("d6 =", d6)
	fmt.Println("d7 =", d7)
	fmt.Println("d8 =", d8)
	fmt.Println("d9 =", d9)
}
