package main

import (
	"fmt"
)

func main() {
	var value1 = true
	var value2 = false

	var valueNot1 = !value1
	//valueNot1 is false

	var valueNot2 = !value2
	//valueNot2 is true

	var valueAnd = value1 && value2
	//valueAnd is false

	var valueOr = value1 || value2
	//valueOr is true

	fmt.Println(valueNot1)
	fmt.Println(valueNot2)
	fmt.Println(valueAnd)
	fmt.Println(valueOr)
}
