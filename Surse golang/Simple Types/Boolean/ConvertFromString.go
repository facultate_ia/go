package main

import (
	"fmt"
	"strconv"
)

func main() {
	var str = "true"
	boolValue, _ := strconv.ParseBool(str)
	//boolValue is true

	fmt.Println(boolValue)
}
