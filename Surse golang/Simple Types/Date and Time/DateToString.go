package main

import (
	"fmt"
	"time"
)

func main() {
	var now = time.Now()

	var shortStyle = now.Format("1/2/06, 3:04 PM")
	//shortStyle is "11/26/18, 4:53 PM"

	var customStyle = now.Format("2006-01-02")
	//customStyle is "2018-11-26"

	fmt.Println(shortStyle)
	fmt.Println(customStyle)
}
