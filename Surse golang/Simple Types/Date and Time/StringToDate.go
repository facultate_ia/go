package main

import (
	"fmt"
	"time"
)

func main() {
	var stringDate = "1945-05-09 01:00"
	var format = "2006-01-02 15:04"
	victoryDate, err := time.Parse(format, stringDate)
	_ = err

	fmt.Println(victoryDate)
}
