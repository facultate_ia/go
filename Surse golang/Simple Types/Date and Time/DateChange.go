package main

import (
	"fmt"
	"time"
)

func main() {
	var now = time.Now()
	var yesterday = now.Add(-24 * time.Hour)
	var tomorrow = now.AddDate(0, 0, 1)
	var nextMonth = now.AddDate(0, 1, 0)
	var nextYear = now.AddDate(1, 0, 0)

	fmt.Println(yesterday.Format("2006-01-02"))
	fmt.Println(tomorrow.Format("2006-01-02"))
	fmt.Println(nextMonth.Format("2006-01-02"))
	fmt.Println(nextYear.Format("2006-01-02"))
}
