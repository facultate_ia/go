package main

import (
	"fmt"
	"time"
)

func main() {
	var now = time.Now()
	var year = now.Year()
	var month = now.Month()
	var day = now.Day()
	var hour = now.Hour()
	var minute = now.Minute()
	var second = now.Second()
	var dayOfWeek = now.Weekday()

	fmt.Println(year)
	fmt.Println(month)
	fmt.Println(day)
	fmt.Println(hour)
	fmt.Println(minute)
	fmt.Println(second)
	fmt.Println(dayOfWeek)
}
