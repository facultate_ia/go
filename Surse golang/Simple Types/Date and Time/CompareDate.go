package main

import (
	"fmt"
	"time"
)

func main() {
	var now = time.Now()
	var yesterday = now.Add(-24 * time.Hour)

	var areEqual = now == yesterday
	//areEqual is false

	var areLater = now.After(yesterday)
	//areLater is true

	var areEarlier = now.Before(yesterday)
	//areEarlier is false

	fmt.Println(areEqual)
	fmt.Println(areLater)
	fmt.Println(areEarlier)
}
