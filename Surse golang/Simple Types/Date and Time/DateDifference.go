package main

import (
	"fmt"
	"time"
)

func main() {
	var victoryDate = time.Date(
		1945, 5, 9, 0, 0, 0, 0, time.UTC)
	var now = time.Now()
	var delta = now.Sub(victoryDate)

	var days = delta.Hours() / 24
	//days is 26865
	var minutes = delta.Minutes()
	//minutes is 3.868567537948e+07
	var seconds = delta.Seconds()
	//seconds is 2.3211405227688e+09

	fmt.Println(days)
	fmt.Println(minutes)
	fmt.Println(seconds)
}
