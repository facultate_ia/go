package main

import (
	"fmt"
	"math"
)

func main() {
	//"const" for constants and
	//"var" or ':=' for variables

	//Int
	number, otherNumber := 42, 37
	const maxInt64 = math.MaxInt32
	const Mb = 1048576

	//Double
	const exp = 2.71828
	var billion = 1E+9

	//Float
	const pi = 3.14

	//String
	const greeting = "Hello"

	//Multiline String
	var text = `this is some
multiline text`

	//Bool
	const sunIsStar = true
	var earthIsStar = false

	//Character "A"
	const charA = 'A' //'\x41'

	fmt.Println(number, otherNumber)
	fmt.Println(maxInt64)
	fmt.Println(Mb)
	fmt.Println(billion)
	fmt.Println(pi)
	fmt.Println(greeting)
	fmt.Println(text)
	fmt.Println(sunIsStar)
	fmt.Println(earthIsStar)
	fmt.Println(charA)
}
