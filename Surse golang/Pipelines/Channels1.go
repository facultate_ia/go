package main

import (
	"fmt"
)

func main() {
	nums := make(chan int, 2) // buffer size 2
	nums <- 1                 // succeeds
	nums <- 3                 // succeeds
	nums <- 5                 // <-deadlock error

	fmt.Println("finish")
}
