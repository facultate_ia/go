package main

import (
	"fmt"
)

//converts a list of strings to a channel
func stringChain(done <-chan struct{}, strs ...string) <-chan string {
	out := make(chan string)
	go func() {
		for _, s := range strs {
			select {
			case out <- s:
			case <-done:
				return
			}
		}
		close(out)
	}()
	return out
}

func main() {
	// Set up the pipeline.
	done := make(chan struct{})
	flowers := stringChain(done, "rose", "astra")

	fmt.Println(<-flowers) // "rose"
	// Tell the remaining senders we're leaving.
	done <- struct{}{}

	//fmt.Println(<-flowers) //<-Error
}
