package main

import (
	"fmt"
	"math"
)

func main() {
	var powOfTwo = func(power float64) float64 {
		return math.Pow(2, power)
	}
	var pow8 = powOfTwo(8)
	//pow8 is 256

	fmt.Println(pow8)
}
