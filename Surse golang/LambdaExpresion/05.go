package main

import (
	"fmt"
)

func checkAndProcess(number int, process func(int)) {
	if number < 10 {
		process(number)
	}
}

func main() {

	var process = func(number int) {
		fmt.Println(number * 10)
	}
	checkAndProcess(5, process)
	//printed: 50
}
