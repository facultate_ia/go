package main

import (
	"fmt"
)

func main() {
	var x = 5
	var addYtoX = func(y int) {
		x += y
	}

	addYtoX(3)
	//x is 8

	fmt.Println(x)
}
