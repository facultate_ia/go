package main

import (
	"fmt"
)

func main() {
	var MakeSum = func() func(int, int) int {
		return func(a, b int) int {
			return a + b
		}
	}

	var sumFunc = MakeSum()
	var sum = sumFunc(5, 8)
	//sum is 13

	fmt.Println(sum)
}
