package main

import (
	"fmt"
)

func main() {
	var avgFunc = func(a, b int) int {
		return (a + b) / 2
	}

	var avg = avgFunc(3, 5)
	//avg is 4

	fmt.Println(avg)
}
