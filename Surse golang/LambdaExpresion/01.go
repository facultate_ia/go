package main

import (
	"fmt"
)

func main() {
	var curriedAvg = func(a int) func(int) int {
		return func(b int) int {
			return (a + b) / 2
		}
	}

	var avg3 = curriedAvg(3)
	var result = avg3(5)
	//result is 4 = (3 + 5)/2

	fmt.Println(result)
}
