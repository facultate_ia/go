package main

import (
	"fmt"
)

func main() {
	var add2AndPrint = func(a int) {
		fmt.Println(a + 2)
	}

	add2AndPrint(5)
	//printed 7
}
