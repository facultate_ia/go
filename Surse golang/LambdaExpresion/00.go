package main

import (
	"fmt"
)

func main() {
	var MakeWallet = func(sum int) func(int) int {
		return func(pay int) int {
			sum -= pay
			return sum
		}
	}

	var payFromWallet1 = MakeWallet(1000)
	var payFromWallet2 = MakeWallet(500)
	var balance = payFromWallet1(50)
	//balans is 950

	balance = payFromWallet2(70)
	//balance is 430

	balance = payFromWallet1(150)
	//balans is 800

	fmt.Println(balance)
}
