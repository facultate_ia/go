
package main

func throwPanic(cars []string) {
    if len(cars) == 0 {
        panic("No cars for sale")
    }
    //some implementation...
}

func main() {
    throwPanic([]string{})
}
Try it in Playground