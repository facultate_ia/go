
package main

import (
    "errors"
    "fmt"
)

func throwWhenNullOrEmpty(arr []int) error {
    if arr == nil {
        return errors.New("Array is nil")
    }
    if len(arr) == 0 {
        return errors.New("Array is empty")
    }
    //some implementation...

    return nil
}

func main() {
    err := throwWhenNullOrEmpty(nil)

    if err != nil {
        fmt.Println(err)
        //printed: Array is nil
    }
}
Try it in Playground