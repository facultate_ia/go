
package main

import (
    "fmt"
)

type simpleException struct {
    errText string
}

func (e simpleException) Error() string {
    return e.errText
}

func throwSimpleException() error {
    return simpleException{"simple exeption"}
}

func main() {
    err := throwSimpleException()

    if err != nil {
        fmt.Println(err)
        //printed: simple exeption
    }
}
Try it in Playground