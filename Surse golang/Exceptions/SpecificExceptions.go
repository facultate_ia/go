
package main

import (
    "errors"
    "fmt"
)

func throwIfNullOrEmpty(arr []int) error {
    if arr == nil {
        return errors.New("Array is nil")
    }
    if len(arr) == 0 {
        return errors.New("Array is empty")
    }
    //some implementation...

    return nil
}

func main() {
    err := throwIfNullOrEmpty(nil)

    if err.Error() == "Array is nil" {
        fmt.Println("Error: array is not specified")
    } else if err.Error() == "Array is empty" {
        fmt.Println("Error: array is empty")
    }
}
Try it in Playground