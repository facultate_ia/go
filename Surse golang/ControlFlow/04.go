
var n = -42
//Go doesn't have the ternary operator
var classify = (map[bool]string
    {true: "positive", false: "negative"})[n &gt; 0]
//classify is "negative"

Try it in Playground