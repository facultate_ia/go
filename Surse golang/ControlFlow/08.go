
var numbers = []int{2, 3, 5, 7, 11, 13, 17, 19}
var str = ""
for _, number := range numbers {
    if number &gt; 10 {
        break
    }
    if len(str) &gt; 0 {
        str += "-"
    }
    str += strconv.Itoa(number)

}
//str is "2-3-5-7"
Try it in Playground