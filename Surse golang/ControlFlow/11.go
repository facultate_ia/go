
import (
    "fmt"
)

func containNumber(numbers []int, number int) bool {
    for _, currentNumber := range numbers {
        if currentNumber == number {
            return true
        }
    }
    return false
}

var numbers = []int{1, 2, 3}
var isContain2 = containNumber(numbers, 2)
//isContain2 is true

var isContain4 = containNumber(numbers, 4)
//isContain4 is false

fmt.Println(isContain2)
fmt.Println(isContain4)
Try it in Playground