package main

import (
	"fmt"
)

func printSomeData(printAll bool) {
	printMainData()
	if !printAll {
		return
	}
	printOtherData()
}

func printMainData() {
	fmt.Println("PrintMainData")
}

func printOtherData() {
	fmt.Println("PrintOtherData")
}

func main() {
	printSomeData(true)
	printSomeData(false)
}
