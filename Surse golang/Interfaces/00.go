package main

import "fmt"

//UID is ...
type UID interface {
	GetUID() int
}

type named interface {
	getName() string
}

type flower struct {
	Name string
}

func (f flower) getName() string {
	return f.Name
}

func main() {
	var rose = flower{"Rose"}

	var i interface{} = rose
	_, isPId := i.(UID)
	//isPId is false

	_, isNamed := i.(named)
	//isPId is true

	fmt.Println(isPId)
	fmt.Println(isNamed)
}
