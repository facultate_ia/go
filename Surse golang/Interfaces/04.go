package main

import "fmt"

type vehicle interface {
	getMaxSpeed() int
}

type truck interface {
	vehicle
	getCapacity() int
}

type kamaz5320 struct{}

func (k kamaz5320) getMaxSpeed() int {
	return 85
}

func (k kamaz5320) getCapacity() int {
	return 8000
}

func main() {
	var kamaz = kamaz5320{}
	var vehicle = vehicle(kamaz)
	var maxSpeed = vehicle.getMaxSpeed()
	//maxSpeed is 85

	fmt.Println(maxSpeed)
}
