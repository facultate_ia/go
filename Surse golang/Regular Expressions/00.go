package main

import (
	"fmt"
	"regexp"
)

func main() {
	var data1 = "aaab"
	var data2 = "aaaba"
	var pattern = "^a+b$"

	re := regexp.MustCompile(pattern)
	var b1 = re.MatchString(data1)
	//b1 is true

	var b2 = re.MatchString(data2)
	//b2 is false

	fmt.Println(b1)
	fmt.Println(b2)
}
