package main

import (
	"fmt"
	"regexp"
)

func main() {
	var data = "Pi is equal to 3.14"
	var pattern = "\\d+.\\d+"
	re := regexp.MustCompile(pattern)
	var pi = re.FindString(data)
	//pi is "3.14"

	var numbers = re.FindAllString(data, -1)
	//numbers is ["3.14"]

	fmt.Println(pi)
	fmt.Println(numbers)
}
