package main

import (
	"fmt"
	"regexp"
)

func main() {
	var data = "Pi = 3.14, exponent = 2.718"
	var pattern = "\\d+.\\d+"
	re := regexp.MustCompile(pattern)
	data = re.ReplaceAllString(data, "<f>$0</f>")
	//data is "Pi = <f>3.14</f>, exponent = <f>2.718</f>"

	fmt.Println(data)
}
