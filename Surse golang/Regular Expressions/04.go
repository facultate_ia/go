package main

import (
	"fmt"
	"regexp"
)

func main() {
	var data = "Pi = 3.14, exponent = 2.718"
	var pattern = "\\d+.\\d+"
	re := regexp.MustCompile(pattern)

	var matchs = re.FindAllString(data, -1)
	//matchs is ["3.14", "2.718"]

	fmt.Println(matchs)
}
