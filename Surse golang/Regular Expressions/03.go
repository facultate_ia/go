package main

import (
	"fmt"
	"regexp"
	"strconv"
)

func main() {
	var data = "x77x6Fx72x6Cx64"
	var pattern = "x([0-9A-F]{2})"
	re := regexp.MustCompile(pattern)
	data = re.ReplaceAllStringFunc(data, func(value string) string {
		code, _ := strconv.ParseInt(value[1:3], 16, 16)
		return string(rune(code))
	})
	//data is "world"

	fmt.Println(data)
}
