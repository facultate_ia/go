package main

import (
	"fmt"
	"regexp"
)

func main() {
	var data = "AaaA\r\naaaA"
	var pattern = "(?im)^(a+)$"
	re := regexp.MustCompile(pattern)

	var value = re.FindString(data)
	//value is "aaaA"
	//i - Ignore Case
	//m - Multiline

	fmt.Println(value)
}
